import React, { useState , useEffect} from 'react';
import './interval.css';


const Interval = () =>{

        const addZero = (i) =>{
      if (i<10) {
        i = "0"+ i;
      } return i;
      
    }
  
    const addZeros = (i) =>{
      if (i<=10) {
        i = "00"+ i;
      }if(i<100){
        i = "0" + i;
      } return i;
      
    }
    const array = [];
    let intervalTime = [];
    const [table, setTable] = useState([]);

    const clickHandler = () => {
      
      array.push(Date.now());
      intervalTime.push([array.map((num,key)=>{
        let d = new Date(num); 
        let hours = addZero(d.getHours());
        let minutes = addZero(d.getMinutes());
        let seconds = addZero(d.getSeconds());
        let msecs = addZeros(d.getMilliseconds());
        return <li key={key}>{hours}:{minutes}:{seconds}:{msecs}</li>
      })])
      setTable(intervalTime)

    console.log(intervalTime);
      
      

      
    }

  

  

    return <div>
                    <button onClick={clickHandler}> Interval Time </button><br/>
                                {table}

    </div>
}




export default Interval;